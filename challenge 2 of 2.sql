#input number of days needed to be reported
set @top = 3;


#initializing tables
drop table t1;
drop table t2;
drop table t3;
drop table t4;


#creating table with highest range for each date
create temporary table t1
select d.Date, max(abs(d.Open - d.Close)) as 'range'
from d3 d
group by d.date
;

#creating table of max high price for each date
create temporary table t2
select d.Date, max(d.High) as 'maxHigh'
from d3 d
group by d.date
;

#creating table of time of max high price for each date
create temporary table t3
select t2.date, d.time as maxHt
from d3 d, t2
where t2.maxHigh = d.High and t2.date = d.date
;

#create table sorting by highest range to lowest
create temporary table t4
select t1.date, t1.range, t3.maxHt
from t3
inner join t1 on t1.date = t3.date
order by t1.range desc;


#creating table of the top x days with the highest range and the time when max high price occured on that date
select t5.date as 'Date', t4.range as 'Range', t4.maxHt as 'Time of Max Price'
from t4,
	(select distinct t1.date
	from t3
	inner join t1 on t1.date = t3.date
	order by t1.range desc
    limit 3) t5
where t5.date = t4.date;




Prepare sth from"
select t5.date as 'Date', t4.range as 'Range', t4.maxHt as 'Time of Max Price'
from t4,
	(    select distinct t1.date
	from t3
	inner join t1 on t1.date = t3.date
	order by t1.range desc
    limit ?) t5
where t5.date = t4.date";
execute sth using @top



