#set begining date and time interval in minutes
set @intval = 300,  @beg_time = '2010-10-11 09:00:00';



select `<ticker>` as 'Trading Symbol', sum(((`<close>` + `<high>` +`<low>`) / 3) * `<vol>`) / sum(`<vol>`) as VWAP, date(@beg_time) as 'Date', date_format(@beg_time, '%H:%m:%S') as 'Start', date_format(@beg_time +  interval @intval minute, '%H:%m:%S') as 'End'
	from d2 
	where `<date>` between @beg_time and @beg_time + interval @intval minute
	and `<ticker>` = 'AAPL'
	group by `<ticker>`
