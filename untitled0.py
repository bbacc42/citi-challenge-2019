

#enter full path location, return new file with formated trade data
def get_trade_data(file_loc):
    f_loc = str(file_loc)


    f = open(f_loc,"r")
    txt = f.read().splitlines()
    
    #find all index of lines containing regrssion record in the file
    i_r = [i for i, e in enumerate(txt) if "Regression: Record Publish:" in e]
    
    #find all index of lines containig Type: Trade
    i_trade = [i for i, e in enumerate(txt) if "Regression:   Type: Trade" in e]
    
    #initializing index
    i = 0
    #initialising list of P for price and V for volumn to store data
    tp = []
    tv = []
    #for each index of trade saved in i_trade, go find the nearest record
    #between which find the index for TradePrice and TradeVolumn
    for i in range(len(i_trade)):
        if i != len(i_trade):
            ir_r = i_r.index(i_trade[i] -1)
            ir_fr = ir_r +1
            #finding the nearest record
            ibeg = i_r[ir_r]
            iend = i_r[ir_fr]
            #within the two nearest records, find TradePrice and TradeVolume
            tp.append(
                    ibeg+[i for i, e in enumerate(txt[ibeg:iend]) if "wTradePrice" in e][0]
                    )
            tv.append(
                    ibeg+[i for i, e in enumerate(txt[ibeg:iend]) if "wTradeVolume" in e][0]
                    )
            
            i += 1
        else:
            return print("trade not found")
        
    #writing out data as format required based on line index   
    iw = 0
    tradeop = open("trade_data.txt", "w")
    for iw in range(len(i_trade)):
        tradeop.write(txt[i_trade[iw]-1].replace("Regression: ", "")+ \
                      txt[i_trade[iw]].replace("Regression:", "")+"\n" +\
                      txt[tp[iw]].replace("Regression:", "") + "\n" +\
                      txt[tv[iw]].replace("Regression:", "") + "\n"
                      )
    tradeop.close()
    return print("please see file trade_data.txt for result")
    